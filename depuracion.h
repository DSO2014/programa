// Header file para incluir la macro de depuración "depurar"
#ifndef __DEPURACION_H__
#define __DEPURACION_H__

#define depurar(FMT, ARGS...) fprintf(stderr, "DEBUG INFO: \"%s\":%d:%s() -->  %s = " FMT " (%s)\n", __FILE__, __LINE__,  __FUNCTION__, #ARGS, ## ARGS, FMT);

#endif
