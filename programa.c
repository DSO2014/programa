#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "depuracion.h"

#define TAM_CADENA 1024

char *puntero;  // variable global

int main (int argc, char **argv)
{
	int i; 		// variable local de la funci�n main()
	
	printf("Nombre del ejecutable: %s \n", argv[0] );
	printf("N�mero de argumentos : %d \n", argc-1 );
	
	for (i=1; i<argc; i++)
		printf("\t Argumento %d = \"%s\" \n", i, argv[i] );
		
    //depurar("%p",puntero);     // mostrar el valor de la variable puntero con informaci�n de contexto
	
	puntero[20]='A';  // Posible problema
        
	puntero= (char*) malloc(TAM_CADENA*sizeof(char));
	
	strcpy(puntero, argv[0]);
	strcat(puntero, " es el programa perfecto");
	
	printf("String copiada en puntero: %s \n", puntero);
	
	free(puntero);
	
	exit(0);
}
